/*
  MIT License

  Copyright (c) 2018 Mina Helmi

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation
  the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the
  Software is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef MCAL_DRIVERS_EEPROMI_DRIVER_EEPROMI_FUNCTIONS_H_
#define MCAL_DRIVERS_EEPROMI_DRIVER_EEPROMI_FUNCTIONS_H_


#include "../../basic_includes/custom_types.h"

void EEPROMi_vidInit(void);

void EEPROMi_vidWriteByte(const u8 u8DataCpy, const u16 u16AddrCpy);
void EEPROMi_vidWriteStr(const char* u8StrCpy, u16 u16AddrCpy);
void EEPROMi_vidWriteArray(const void* vidPtrArrCpy, u16 u16LengthCpy, u16 u16AddrCpy);

u8 EEPROMi_u8ReadByte(const u16 u16AddrCpy);
void EEPROMi_vidReadArray(void* vidPtrArrCpy, u16 u16LengthCpy, u16 u16AddrCpy);


#endif /* MCAL_DRIVERS_EEPROMI_DRIVER_EEPROMI_FUNCTIONS_H_ */

